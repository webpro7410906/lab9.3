import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Stock } from './entities/stock.entity';
import { Repository } from 'typeorm';
import { CreateStockDto } from './dto/create-stock.dto';
import { UpdateStockDto } from './dto/update-stock.dto';


@Injectable()
export class StockService {
  constructor(
    @InjectRepository(Stock) private stockRepository: Repository<Stock>,
  ) {}
  create(createStockDto: CreateStockDto) {
    const stock = new Stock();
    stock.name = createStockDto.name;
    stock.price = parseFloat(createStockDto.price);
    stock.type = JSON.parse(createStockDto.type);
    if (createStockDto.image && createStockDto.image !== '') {
      stock.image = createStockDto.image;
    }
    return this.stockRepository.save(stock);
  }

  findAll() {
    return this.stockRepository.find({ relations: { type: true } });
  }

  findOne(id: number) {
    return this.stockRepository.findOne({
      where: { id },
      relations: { type: true },
    });
  }

  async update(id: number, updateStockDto: UpdateStockDto) {
    const stock = await this.stockRepository.findOneOrFail({
      where: { id },
    });
    stock.name = updateStockDto.name;
    stock.price = parseFloat(updateStockDto.price);
    stock.type = JSON.parse(updateStockDto.type);
    if (updateStockDto.image && updateStockDto.image !== '') {
      stock.image = updateStockDto.image;
    }
    this.stockRepository.save(stock);
    const result = await this.stockRepository.findOne({
      where: { id },
      relations: { type: true },
    });
    return result;
  }

  async remove(id: number) {
    const deleteStock = await this.stockRepository.findOneOrFail({
      where: { id },
    });
    await this.stockRepository.remove(deleteStock);

    return deleteStock;
  }
}
